import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './layouts/dashboard/dashboard.component';
import { BlankComponent } from './layouts/blank/blank.component';
import { LoginComponent } from './appviews/login/login.component';
import { AuthGuardService as AuthGuard } from './auth/auth-guard.service';
import { UserDashboardComponent } from './appviews/user/user-dashboard/user-dashboard.component';
import { AdminDashboardComponent } from './appviews/admin/admin-dashboard/admin-dashboard.component';
import { LogoutComponent } from './appviews/logout/logout.component';

const routes: Routes = [
  {path: '', redirectTo: 'login', pathMatch: 'full'},
  {
    path: 'user', component: DashboardComponent,
    children: [
      {path: 'user-dashboard', component: UserDashboardComponent},
      {path: 'admin-dashboard', component: AdminDashboardComponent},
    ],
    canActivate: [AuthGuard]
  },
  {
    path: '', component: BlankComponent,
    children: [
      {path: 'login', component: LoginComponent},
      {path: 'logout', component: LogoutComponent}
    ]
  },
  // Handle all other routes
  {path: '**', redirectTo: 'login'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
